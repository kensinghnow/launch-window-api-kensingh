***************************************************************************
Integration Developer: Ken Singh
Email: kansingh1@gmail.com
Project Title: Documentation for Project - Launch Window
Repository Access: https://bitbucket.org/kensinghnow/launch-window-api-kensingh.git
****************************************************************************

-About the Code and how to run it
1. This project is a Mulesoft API which can be imported into Anypoint Studio. Mule runtime 3.9.2 is required to run this Project.
2. The code has been mavenized and can be imported via pom.xml settings in Studio
3. The code can be deployed into local machine to run on Port 8082 as it's configured for HTTPS
4. Any Rest Client can be used to make call to the API in local environment like Postaman
5. This API is also deployed on Mulesoft Cloudhub and can be accessed via sending email to kansingh1@gmail.com

-Best Practices enabled in this Project
1. Mulesoft Development best practices have been implemented in this API.
2. Project has been divided into different configuration files to demonstrate the easy to read coding practice.
3. Nouns have been used to name the Flows / Variables / Functions / Connectors in Mule API.
4. Application has been configured to support different properties file for each environment like Dev / SIT / Prod.
5. API has been configured on Port 8082 with HTTPS and TLSv1.2.
6. Java AES 128Bytes Encryption teachnology have been used to encrypt the sestive username and passwords.
7. Logging has been enables in the Project.
8. Re-usability of code is demonstrated this project by re-using different sub-flows.
9. Code Versioning has been maintained.
10. Performance of the Application is taken into care during development and proper allocation of vCores in Cloudhub.
11. Business rules in this API has been kept in separate file for easy change in the future rather than hard coding anywhere in application.

-Improvements which can be made to this Project(due to restricted timelines cannot be achieved right now) as recommended by me
1. API Led Connectivity can be the approach as follows:
     - System API calling Weather API --> (kept secured within Cloudhub- VPC)
     - Process API doing all the transformation and calculation (kept secured within Cloudhub VPC)
     - Experience API can be exposed outside VPC to public Interface
2. MUnit/JUnit Tests should be included.
3. Caching technique can be enabled.
4. Security Policies in Mule Platform can be enabled like ClientID Enforcement or OAuth2.0 or IP Whitelisting
5. Rate Limiting should be enabled to protect the API
6. API should be configured for proper monitoring with Anypoint Monitoring


